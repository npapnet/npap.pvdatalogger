﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;


namespace npap.lib.WebRequests
{
    public class npapWebRequests
    {
        private static int LoggingLevel = 0;
        /// <summary>
        /// Sample of usage
        /// start ("http://weather.noaa.gov/weather/current/LGIR.html");
        /// npap.lib.WebRequests.npapWebRequests.start("http://weather.aero/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&stationString=LGIR&hoursBeforeNow=20");
        /// http://weather.aero/tools/dataservices/textdataserver/dataproducts/view/product/metars/section/examples
        /// http://weather.aero/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&stationString=LGIR&hoursBeforeNow=20
        /// </summary>

        public static string start_post(string baseURL, byte[] buffer )
        {
            //Our postvars
            //byte[] buffer = Encoding.ASCII.GetBytes("test=postvar&test2=another");
            //baseURL = "http://www.mechatron.eu";
            //Initialization
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(baseURL);
            //Our method is post, otherwise the buffer (postvars) would be useless
            WebReq.Method = "POST";
            //We use form contentType, for the postvars.
            WebReq.ContentType = "application/x-www-form-urlencoded";
            //The length of the buffer (postvars) is used as contentlength.
            WebReq.ContentLength = buffer.Length;
            //We open a stream for writing the postvars
            Stream PostData = WebReq.GetRequestStream();
            //Now we write, and afterwards, we close. Closing is always important!
            PostData.Write(buffer, 0, buffer.Length);
            PostData.Close();
            //Get the response handle, we have no true response yet!
            HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
            //Let's show some information about the response
            
            //Now, we read the response (the string), and output it.
            StreamReader _Answer = new StreamReader(WebResp.GetResponseStream());
            string strResponse = _Answer.ReadToEnd();
    
            switch(LoggingLevel)
            {
                case 2:
                    Console.WriteLine(strResponse);
                    goto case 1;
                case 1:
                    Console.WriteLine(WebResp.StatusCode);
                    Console.WriteLine(WebResp.Server);
                    goto case 0;
                case 0: 
                default:
                    Console.WriteLine("Data from " + baseURL);
                    break;
            }
            return (strResponse);


        }
        public static string start_get(string baseURL, string getVars)
        {
            //Our getVars, to test the get of our php. 
            //We can get a page without any of these vars too though.
            //baseURL = "http://www.wunderground.com/Aviation/index.html?";
            //getVars = "query=LGIR";
            //Initialization
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format(baseURL, getVars));
            //This time, our method is GET.
            WebReq.Method = "GET";
            //From here on, it's all the same as above.
            HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

            //Now, we read the response (the string), and output it.
            StreamReader _Answer = new StreamReader(WebResp.GetResponseStream());

            string answerString = _Answer.ReadToEnd();
            switch (LoggingLevel)
            {
                case 2:
                    Console.WriteLine(answerString);
                    goto case 1;
                case 1:
                    Console.WriteLine(WebResp.StatusCode);
                    Console.WriteLine(WebResp.Server);
                    goto case 0;
                case 0:
                default:
                    Console.WriteLine("Data from " + baseURL);
                    break;
            }

            return answerString;
        }

        public static string start(string baseURL)
        {
            //Initialization
            //HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format("http://www.mechatron.eu", getVars));
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format(baseURL));
            //From here on, it's all the same as above.
            HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
            //Let's show some information about the response
            Console.WriteLine(WebResp.StatusCode);
            Console.WriteLine(WebResp.Server);

            //Now, we read the response (the string), and output it.
            StreamReader _Answer = new StreamReader(WebResp.GetResponseStream());
            string answerString = _Answer.ReadToEnd();
            switch (LoggingLevel)
            {
                case 2:
                    Console.WriteLine(answerString);
                    goto case 1;
                case 1:
                    Console.WriteLine(WebResp.StatusCode);
                    Console.WriteLine(WebResp.Server);
                    goto case 0;
                case 0:
                default:
                    Console.WriteLine("Data from " + baseURL);
                    break;
            }

            //Console.WriteLine(answerString);
            return (answerString);
        }

    }
}
