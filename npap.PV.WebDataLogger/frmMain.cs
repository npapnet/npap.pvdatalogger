﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace npap.PV.WebDataLogger
{
    public partial class frmMain : Form
    {

        #region Variables and Constants
        PVLoggerSystems pvls = new PVLoggerSystems();
        DataTable _dt; 
        #endregion

        #region Constructor and Initialisation
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            _dt = pvls.getSystemListDataTable();
            this.RefreshSystemListBox(_dt);
        } 
        #endregion

        private void RefreshSystemListBox(DataTable _dt)
        {
            lbSiteList.DataSource = _dt;
            lbSiteList.DisplayMember = "Description";
            lbSiteList.ValueMember = "URI";
        }


        private void btnAddSite_Click(object sender, EventArgs e)
        {
            //npap.lib.WebRequests.npapWebRequests.start(tbSiteURL.Text);
            try
            {
                string Description = "";
                DataTable dt = pvls.AddSite(tbSiteURL.Text,Description);
                lbSiteList.DataSource = dt;
                lbSiteList.DisplayMember = "Description";
                lbSiteList.ValueMember = "URI";
            }
            catch (Exception)
            { }
        }

        private void btnSiteForm_Click(object sender, EventArgs e)
        {
            MessageBox.Show((string)lbSiteList.SelectedValue);
            //pvls.ReturnCurrentSite(lbSiteList.SelectedValue);
         /* TODO : What will happen when a form is selected. 
          * currentSite = (npap.lib.WebEnabledPVDataLogger.PVDL_t1)lbSiteList.SelectedValue;
            currentSite.VisibleSiteForm = !currentSite.VisibleSiteForm;
            if (currentSite.VisibleSiteForm)
            {
                currentSite.getWebData();
            }*/
        }

        private void lbSiteList_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                DataRowView drv=(DataRowView)lbSiteList.SelectedItem;
                tbSiteDescription.Text = drv.Row.ToString();
                tbSiteURL.Text = (string)lbSiteList.SelectedValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


    }
}
