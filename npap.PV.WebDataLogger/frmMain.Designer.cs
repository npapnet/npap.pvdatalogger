﻿namespace npap.PV.WebDataLogger
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbSiteURL = new System.Windows.Forms.TextBox();
            this.lbSiteURL = new System.Windows.Forms.Label();
            this.btnAddSite = new System.Windows.Forms.Button();
            this.lbSiteList = new System.Windows.Forms.ListBox();
            this.btnDeleteSite = new System.Windows.Forms.Button();
            this.btnSiteForm = new System.Windows.Forms.Button();
            this.tbSiteDescription = new System.Windows.Forms.TextBox();
            this.lblSiteDescription = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbSiteURL
            // 
            this.tbSiteURL.Location = new System.Drawing.Point(59, 37);
            this.tbSiteURL.Name = "tbSiteURL";
            this.tbSiteURL.Size = new System.Drawing.Size(555, 20);
            this.tbSiteURL.TabIndex = 0;
            // 
            // lbSiteURL
            // 
            this.lbSiteURL.AutoSize = true;
            this.lbSiteURL.Location = new System.Drawing.Point(6, 40);
            this.lbSiteURL.Name = "lbSiteURL";
            this.lbSiteURL.Size = new System.Drawing.Size(47, 13);
            this.lbSiteURL.TabIndex = 1;
            this.lbSiteURL.Text = "SiteURL";
            // 
            // btnAddSite
            // 
            this.btnAddSite.Location = new System.Drawing.Point(264, 75);
            this.btnAddSite.Name = "btnAddSite";
            this.btnAddSite.Size = new System.Drawing.Size(122, 35);
            this.btnAddSite.TabIndex = 2;
            this.btnAddSite.Text = "Add Site";
            this.btnAddSite.UseVisualStyleBackColor = true;
            this.btnAddSite.Click += new System.EventHandler(this.btnAddSite_Click);
            // 
            // lbSiteList
            // 
            this.lbSiteList.FormattingEnabled = true;
            this.lbSiteList.Location = new System.Drawing.Point(392, 75);
            this.lbSiteList.Name = "lbSiteList";
            this.lbSiteList.Size = new System.Drawing.Size(221, 121);
            this.lbSiteList.TabIndex = 3;
            this.lbSiteList.SelectedIndexChanged += new System.EventHandler(this.lbSiteList_SelectedIndexChanged);
            // 
            // btnDeleteSite
            // 
            this.btnDeleteSite.Location = new System.Drawing.Point(265, 116);
            this.btnDeleteSite.Name = "btnDeleteSite";
            this.btnDeleteSite.Size = new System.Drawing.Size(121, 35);
            this.btnDeleteSite.TabIndex = 4;
            this.btnDeleteSite.Text = "Delete Site";
            this.btnDeleteSite.UseVisualStyleBackColor = true;
            // 
            // btnSiteForm
            // 
            this.btnSiteForm.Location = new System.Drawing.Point(265, 161);
            this.btnSiteForm.Name = "btnSiteForm";
            this.btnSiteForm.Size = new System.Drawing.Size(121, 35);
            this.btnSiteForm.TabIndex = 4;
            this.btnSiteForm.Text = "Open Site Form";
            this.btnSiteForm.UseVisualStyleBackColor = true;
            this.btnSiteForm.Click += new System.EventHandler(this.btnSiteForm_Click);
            // 
            // tbSiteDescription
            // 
            this.tbSiteDescription.Location = new System.Drawing.Point(90, 11);
            this.tbSiteDescription.Name = "tbSiteDescription";
            this.tbSiteDescription.Size = new System.Drawing.Size(524, 20);
            this.tbSiteDescription.TabIndex = 0;
            // 
            // lblSiteDescription
            // 
            this.lblSiteDescription.AutoSize = true;
            this.lblSiteDescription.Location = new System.Drawing.Point(6, 14);
            this.lblSiteDescription.Name = "lblSiteDescription";
            this.lblSiteDescription.Size = new System.Drawing.Size(81, 13);
            this.lblSiteDescription.TabIndex = 5;
            this.lblSiteDescription.Text = "Site Description";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 446);
            this.Controls.Add(this.lblSiteDescription);
            this.Controls.Add(this.btnSiteForm);
            this.Controls.Add(this.btnDeleteSite);
            this.Controls.Add(this.lbSiteList);
            this.Controls.Add(this.btnAddSite);
            this.Controls.Add(this.lbSiteURL);
            this.Controls.Add(this.tbSiteDescription);
            this.Controls.Add(this.tbSiteURL);
            this.Name = "frmMain";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbSiteURL;
        private System.Windows.Forms.Label lbSiteURL;
        private System.Windows.Forms.Button btnAddSite;
        private System.Windows.Forms.ListBox lbSiteList;
        private System.Windows.Forms.Button btnDeleteSite;
        private System.Windows.Forms.Button btnSiteForm;
        private System.Windows.Forms.TextBox tbSiteDescription;
        private System.Windows.Forms.Label lblSiteDescription;
    }
}

