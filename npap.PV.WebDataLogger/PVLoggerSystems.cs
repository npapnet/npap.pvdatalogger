﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace npap.PV.WebDataLogger
{
        
    public class PVLoggerSystems
    {
        #region Variables Constants
        #region SQL Related
        SqlConnection cn = new SqlConnection("Data Source=dellt1500\\sqlexpress;Initial Catalog=npapPVlogger;Integrated Security=True");
        SqlDataAdapter adp = null;
        DataTable dtSystemsList = new DataTable("SystemsList");
        string qry = @"
select *
from dbo.Systems"; 
        #endregion

        Dictionary<string, npap.lib.WebEnabledPVDataLogger.PVDL_t1> dicSites;
        npap.lib.WebEnabledPVDataLogger.PVDL_t1 currentSite;

        #endregion

        #region Constructor
        public PVLoggerSystems()
        {
            adp = new SqlDataAdapter(qry, cn);
        }
        
        #endregion



        public DataTable getSystemListDataTable()
        {
            /* dicSites = new Dictionary<string,lib.WebEnabledPVDataLogger.PVDL_t1>();
 currentSite = new npap.lib.WebEnabledPVDataLogger.PVDL_t1(tbSiteURL.Text);
 dicSites.Add(tbSiteURL.Text, currentSite);
 lbSiteList.DataSource = new BindingSource(dicSites, null);
 lbSiteList.DisplayMember = "Key";
 lbSiteList.ValueMember = "Value";*/
            try
            {
                adp.Fill(dtSystemsList);
            }
            catch (Exception ex)
            {

            }
            finally { cn.Close(); }
            return dtSystemsList;

        }

        /// <summary>
        /// Add site to dictionary Sites and also adds data.
        /// </summary>
        /// <param name="strURI"></param>
        /// <param name="siteDescription"></param>
        /// <returns></returns>
        internal DataTable AddSite(string strURI, string siteDescription)
        {
            currentSite = new npap.lib.WebEnabledPVDataLogger.PVDL_t1(strURI);
            dicSites.Add(strURI, currentSite);

            return dtSystemsList;
        }

        internal void ReturnCurrentSite(object p)
        {
            throw new NotImplementedException();
        }
    }
}
