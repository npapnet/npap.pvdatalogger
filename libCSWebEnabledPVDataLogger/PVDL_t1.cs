﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace npap.lib.WebEnabledPVDataLogger
{
    public class PVDL_t1
    {
        public Uri uriBaseSite { get; set; }
        private Uri uriBaseVars;
        private Uri uriCurrentData;
        private frmSiteInfo frmThisSite;

        public PVDL_t1 (string siteURL)
        {
            frmThisSite = new frmSiteInfo();
            uriBaseSite = new Uri(siteURL);
            uriBaseVars = new Uri(uriBaseSite.GetLeftPart(UriPartial.Path) + "/base_vars.js");
            uriCurrentData = new Uri(uriBaseSite.GetLeftPart(UriPartial.Path) + "/days.js");

            //getWebData();
            //frmThisSite.Visible = true;
            frmThisSite.Text = uriBaseSite.ToString();
        }

        public void getWebData()
        {
            Console.WriteLine("Base Site URI: {0}", uriBaseSite.AbsoluteUri);
            Console.WriteLine("Base Vars URI: {0}", uriBaseVars.AbsoluteUri);
            Console.WriteLine("Current Vars URI: {0}", uriCurrentData.AbsoluteUri);
            frmThisSite.setBaseVars(npap.lib.WebRequests.npapWebRequests.start(uriBaseVars.AbsoluteUri));
            frmThisSite.setCurrentData(npap.lib.WebRequests.npapWebRequests.start_get(uriCurrentData.AbsoluteUri, ""));
        }

        public bool VisibleSiteForm
        {
            get { return (frmThisSite.Visible);}
            set { frmThisSite.Visible=value;}
        }


    }
}
