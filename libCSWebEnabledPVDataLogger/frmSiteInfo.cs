﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace npap.lib.WebEnabledPVDataLogger
{
    public partial class frmSiteInfo : Form
    {


        public frmSiteInfo()
        {
            InitializeComponent();
        }

        private void frmSiteInfo_Load(object sender, EventArgs e)
        {

        }

        internal void setBaseVars(string baseVarsString)
        {
            tbBaseVars.Text = baseVarsString;
        }

        internal void setCurrentData(string DataString)
        {
            tbCurrentInfo.Text = DataString;
        }

        private void frmSiteInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true; //
        }

        private void btnRefreshForm_Click(object sender, EventArgs e)
        {
            //TODO: Create an event handler for this button, that will interact with PVDL_t1
        }



    }
}
