﻿namespace npap.lib.WebEnabledPVDataLogger
{
    partial class frmSiteInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbBaseVars = new System.Windows.Forms.TextBox();
            this.lblBasicConfig = new System.Windows.Forms.Label();
            this.tbCurrentInfo = new System.Windows.Forms.TextBox();
            this.lblCurrentData = new System.Windows.Forms.Label();
            this.pnlText = new System.Windows.Forms.Panel();
            this.spltHorizontal = new System.Windows.Forms.SplitContainer();
            this.btnRefreshForm = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tblpnlStringData = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlText.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltHorizontal)).BeginInit();
            this.spltHorizontal.Panel1.SuspendLayout();
            this.spltHorizontal.Panel2.SuspendLayout();
            this.spltHorizontal.SuspendLayout();
            this.tblpnlStringData.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbBaseVars
            // 
            this.tbBaseVars.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbBaseVars.Location = new System.Drawing.Point(3, 20);
            this.tbBaseVars.Multiline = true;
            this.tbBaseVars.Name = "tbBaseVars";
            this.tbBaseVars.ReadOnly = true;
            this.tbBaseVars.Size = new System.Drawing.Size(325, 281);
            this.tbBaseVars.TabIndex = 0;
            // 
            // lblBasicConfig
            // 
            this.lblBasicConfig.AutoSize = true;
            this.lblBasicConfig.Location = new System.Drawing.Point(3, 8);
            this.lblBasicConfig.Name = "lblBasicConfig";
            this.lblBasicConfig.Size = new System.Drawing.Size(98, 13);
            this.lblBasicConfig.TabIndex = 1;
            this.lblBasicConfig.Text = "Basic Configuration";
            // 
            // tbCurrentInfo
            // 
            this.tbCurrentInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbCurrentInfo.Location = new System.Drawing.Point(334, 20);
            this.tbCurrentInfo.Multiline = true;
            this.tbCurrentInfo.Name = "tbCurrentInfo";
            this.tbCurrentInfo.ReadOnly = true;
            this.tbCurrentInfo.Size = new System.Drawing.Size(326, 281);
            this.tbCurrentInfo.TabIndex = 2;
            // 
            // lblCurrentData
            // 
            this.lblCurrentData.AutoSize = true;
            this.lblCurrentData.Location = new System.Drawing.Point(223, 9);
            this.lblCurrentData.Name = "lblCurrentData";
            this.lblCurrentData.Size = new System.Drawing.Size(67, 13);
            this.lblCurrentData.TabIndex = 1;
            this.lblCurrentData.Text = "Current Data";
            // 
            // pnlText
            // 
            this.pnlText.Controls.Add(this.spltHorizontal);
            this.pnlText.Controls.Add(this.lblBasicConfig);
            this.pnlText.Controls.Add(this.lblCurrentData);
            this.pnlText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlText.Location = new System.Drawing.Point(0, 0);
            this.pnlText.Name = "pnlText";
            this.pnlText.Size = new System.Drawing.Size(663, 365);
            this.pnlText.TabIndex = 3;
            // 
            // spltHorizontal
            // 
            this.spltHorizontal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltHorizontal.Location = new System.Drawing.Point(0, 0);
            this.spltHorizontal.Name = "spltHorizontal";
            this.spltHorizontal.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltHorizontal.Panel1
            // 
            this.spltHorizontal.Panel1.Controls.Add(this.tblpnlStringData);
            // 
            // spltHorizontal.Panel2
            // 
            this.spltHorizontal.Panel2.Controls.Add(this.btnRefreshForm);
            this.spltHorizontal.Size = new System.Drawing.Size(663, 365);
            this.spltHorizontal.SplitterDistance = 304;
            this.spltHorizontal.SplitterWidth = 10;
            this.spltHorizontal.TabIndex = 4;
            // 
            // btnRefreshForm
            // 
            this.btnRefreshForm.Enabled = false;
            this.btnRefreshForm.Location = new System.Drawing.Point(3, 3);
            this.btnRefreshForm.Name = "btnRefreshForm";
            this.btnRefreshForm.Size = new System.Drawing.Size(121, 48);
            this.btnRefreshForm.TabIndex = 3;
            this.btnRefreshForm.Text = "button1";
            this.btnRefreshForm.UseVisualStyleBackColor = true;
            this.btnRefreshForm.Click += new System.EventHandler(this.btnRefreshForm_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Base Vars";
            // 
            // tblpnlStringData
            // 
            this.tblpnlStringData.ColumnCount = 2;
            this.tblpnlStringData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblpnlStringData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblpnlStringData.Controls.Add(this.tbCurrentInfo, 1, 1);
            this.tblpnlStringData.Controls.Add(this.label1, 0, 0);
            this.tblpnlStringData.Controls.Add(this.tbBaseVars, 0, 1);
            this.tblpnlStringData.Controls.Add(this.label2, 1, 0);
            this.tblpnlStringData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblpnlStringData.Location = new System.Drawing.Point(0, 0);
            this.tblpnlStringData.Name = "tblpnlStringData";
            this.tblpnlStringData.RowCount = 2;
            this.tblpnlStringData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.592105F));
            this.tblpnlStringData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 94.4079F));
            this.tblpnlStringData.Size = new System.Drawing.Size(663, 304);
            this.tblpnlStringData.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(334, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "CurrentInfo";
            // 
            // frmSiteInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 365);
            this.Controls.Add(this.pnlText);
            this.Name = "frmSiteInfo";
            this.Text = "frmSiteInfo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSiteInfo_FormClosing);
            this.Load += new System.EventHandler(this.frmSiteInfo_Load);
            this.pnlText.ResumeLayout(false);
            this.pnlText.PerformLayout();
            this.spltHorizontal.Panel1.ResumeLayout(false);
            this.spltHorizontal.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltHorizontal)).EndInit();
            this.spltHorizontal.ResumeLayout(false);
            this.tblpnlStringData.ResumeLayout(false);
            this.tblpnlStringData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbBaseVars;
        private System.Windows.Forms.Label lblBasicConfig;
        private System.Windows.Forms.TextBox tbCurrentInfo;
        private System.Windows.Forms.Label lblCurrentData;
        private System.Windows.Forms.Panel pnlText;
        private System.Windows.Forms.Button btnRefreshForm;
        private System.Windows.Forms.SplitContainer spltHorizontal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tblpnlStringData;
        private System.Windows.Forms.Label label2;
    }
}